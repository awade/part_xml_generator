#excel to xml
import os
import argparse
from posixpath import split
from unicodedata import name
import pandas as pd
import xml.etree.ElementTree as ET


#Command Line Argument Parser
parser = argparse.ArgumentParser(description="input parameters for xlsx to XML conversion tool", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("src", help="HPK parts spreadsheet file path")
parser.add_argument("template", help="XML template for the batch of parts. Currently this script can only handle Full Sensors NOT MGW.")
parser.add_argument("-n", "--nSheets", action="append", default=None, type=int, help="Specific sheet to be converted to XML. Default is 'None' which reads all sheets. To select single sheets use integer values beginning at 0. To select multiple add an additional '-n' argument for each sheet e.g. '-n 0 -n 1' for sheets 1 and 2")
parser.add_argument("-d", "--debug", default=0, type=int, help="For debug print outs use 1.")
parser.add_argument("-c", "--campaign", default=None, help="Currently only used to switch to old google doc format. Options are None, preseries, preproduction, or production")
args = vars(parser.parse_args())

#Parameters input by users
srcfile = args["src"]
shtName = args["nSheets"]
waferTemplate = args["template"]
debugState = args["debug"] 
campaign = args["campaign"]
   

#Loads XML Wafer Template 
#Currently hard coded for Full Size Wafers only.
waferTemplate = os.path.abspath(waferTemplate)
if debugState == 1:
    print("wafer template ",waferTemplate)
wafertemplatetree = ET.parse(waferTemplate)
#wafertemplatetree = ET.parse('./200umWaferTemplates/200umLDFullalternate.xml')
root = wafertemplatetree.getroot()
printable = ET.tostring(root, "utf-8")

#This Section searches the template for xml tags and assigns them a varaible. Excel data will be assigned to the 'text' part of these variable to populate the XML tags with data. 

#Wafer XML Tag variables
wafer_type = root.find("*/*/KIND_OF_PART")
wafer_ID = root.find("*/*/SERIAL_NUMBER")
wafer_name = root.find("*/*/NAME_LABEL")
wafer_batch = root.find("*/*/BATCH_NUMBER")
wafer_comment = root.find("*/*/COMMENT_DESCRIPTION")
wafer_manufacturer = root.find("*/*/MANUFACTURER")
#sensor tags 

sensor_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[0]
sensor_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[0]
sensor_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[0]
sensor_location = root.findall("*/*/CHILDREN/*/LOCATION")[0]
#sensor_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[0]
sensor_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[0] 

#Create Sensor & Halfmoon XML Tag Variables
if "Full" in sensor_type.text:
    snsrtyp = "Full"
    halfmoon_TOP_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[1]
    halfmoon_TOP_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[1]
    halfmoon_TOP_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[1]
    halfmoon_TOP_location = root.findall("*/*/CHILDREN/*/LOCATION")[1]
    #halfmoon_TOP_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[1]
    halfmoon_TOP_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[1]
    
    halfmoon_BOT_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[2]
    halfmoon_BOT_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[2]
    halfmoon_BOT_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[2]
    halfmoon_BOT_location = root.findall("*/*/CHILDREN/*/LOCATION")[2]
    #halfmoon_BOT_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[2]
    halfmoon_BOT_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[2]
    
    halfmoon_UL_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[3]
    halfmoon_UL_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[3]
    halfmoon_UL_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[3]
    halfmoon_UL_location = root.findall("*/*/CHILDREN/*/LOCATION")[3]
    #halfmoon_UL_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[3]
    halfmoon_UL_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[3]

    halfmoon_LL_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[4]
    halfmoon_LL_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[4]
    halfmoon_LL_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[4]
    halfmoon_LL_location = root.findall("*/*/CHILDREN/*/LOCATION")[4]
    #halfmoon_LL_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[4]
    halfmoon_LL_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[4]

    halfmoon_LR_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[5]
    halfmoon_LR_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[5]
    halfmoon_LR_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[5]
    halfmoon_LR_location = root.findall("*/*/CHILDREN/*/LOCATION")[5]
    # halfmoon_LR_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[5]
    halfmoon_LR_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[5]

    halfmoon_UR_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[6]
    halfmoon_UR_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[6]
    halfmoon_UR_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[6]
    halfmoon_UR_location = root.findall("*/*/CHILDREN/*/LOCATION")[6]
    # halfmoon_UR_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[6]
    halfmoon_UR_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[6]
    if debugState == 1:
        print(halfmoon_UR_manufacturer.text)
    
else: #MGW xml template will point the program here
    sensor2_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[1]
    sensor2_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[1]
    sensor2_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[1]
    sensor2_location = root.findall("*/*/CHILDREN/*/LOCATION")[1]
    # sensor2_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[1]
    sensor2_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[1] 
    
    halfmoon_TOP_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[2]
    halfmoon_TOP_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[2]
    halfmoon_TOP_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[2]
    halfmoon_TOP_location = root.findall("*/*/CHILDREN/*/LOCATION")[2]
    # halfmoon_TOP_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[2]
    halfmoon_TOP_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[2]
    
    halfmoon_BOT_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[3]
    halfmoon_BOT_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[3]
    halfmoon_BOT_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[3]
    halfmoon_BOT_location = root.findall("*/*/CHILDREN/*/LOCATION")[3]
    # halfmoon_BOT_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[3]
    halfmoon_BOT_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[3]
    
    halfmoon_UL_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[4]
    halfmoon_UL_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[4]
    halfmoon_UL_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[4]
    halfmoon_UL_location = root.findall("*/*/CHILDREN/*/LOCATION")[4]
    # halfmoon_UL_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[4]
    halfmoon_UL_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[4]

    halfmoon_LL_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[5]
    halfmoon_LL_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[5]
    halfmoon_LL_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[5]
    halfmoon_LL_location = root.findall("*/*/CHILDREN/*/LOCATION")[5]
    # halfmoon_LL_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[5]
    halfmoon_LL_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[5]

    halfmoon_LR_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[6]
    halfmoon_LR_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[6]
    halfmoon_LR_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[6]
    halfmoon_LR_location = root.findall("*/*/CHILDREN/*/LOCATION")[6]
    # halfmoon_LR_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[6]
    halfmoon_LR_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[6]

    halfmoon_UR_type = root.findall("*/*/CHILDREN/*/KIND_OF_PART")[7]
    halfmoon_UR_ID = root.findall("*/*/CHILDREN/*/SERIAL_NUMBER")[7]
    halfmoon_UR_name = root.findall("*/*/CHILDREN/*/NAME_LABEL")[7]
    halfmoon_UR_location = root.findall("*/*/CHILDREN/*/LOCATION")[7]
    # halfmoon_UR_institution = root.findall("*/*/CHILDREN/*/INSTITUTION")[7]
    halfmoon_UR_manufacturer = root.findall("*/*/CHILDREN/*/MANUFACTURER")[7]



#Pull data from Excel and assign it to the XML tag variables

#this is designed for the current HGCAL google doc format. Will need adaptation for HPK formatting
srcfile = os.path.abspath(srcfile) #ensures absolute path is used for excel file location
if debugState == 1:
    print("excel source ",srcfile)

excelFile = pd.ExcelFile(srcfile) #read excel file to dataframe. Single sheet files will be read as df. Multi-sheet files will be read as dict of df's.

#this block creates a list of sheets based on user input parameter -n to parse
allsheets = excelFile.sheet_names
sheets = [None for _ in shtName]
y=0
for x in shtName:
    print("x=",x)    
    print("allsheets[x]=",allsheets[x])    
    sheets[y] = (allsheets[x])
    y=y+1 
#The excel formate for MGW sensors currently occupy two or three lines per wafer eg row 1 is the top sensor row 2 is the bottom. This loop will try to make two separate wafers for each row
#New loop may need to be made to accomodate these issues   
    #loop over Excel sheets

for n in sheets: #loop through sheets given in the excel file. Default to all sheets, but arg -n can provide a list of integers to select sheets
    n_int = sheets.index(n) #integer index of the current sheet being parsed
    if debugState == 1:
        print(n_int,  "n_int ")
    single_sheet = pd.read_excel(srcfile, sheet_name=n, header=1, usecols='A:H')
    if debugState ==1:
        print(single_sheet)
    #loop through row in excel sheet rows correspond to wafers
    thick_dens = sheets[n_int].split(" ")
    thickness = thick_dens[0]
    density = thick_dens[1]
    for m, row in single_sheet.iterrows():
        if pd.isnull(single_sheet.at[m, 'Sensor ID']):
            print('skipping empty row #', m)
        else:
            print(str(single_sheet.at[m, 'Sensor ID']))
            wafer_type.text = ' '.join([thickness, density,"Si Sensor", "Wafer"])
            if campaign=='preseries':
                wafer_name.text = str(single_sheet.at[m,'Sensor ID'])
                wafer_ID.text = str(single_sheet.at[m,'Scratch pad ID'])
                if pd.isnull(single_sheet.at[m,'OBA Number']):
                    wName_split = wafer_name.text.split('_')
                    wafer_batch.text = wName_split[0]
                else:
                    wafer_batch.text = str(single_sheet.at[m,'OBA Number'])
            else:
                wafer_ID.text = str(single_sheet.at[m,'Sensor ID'])
                wafer_name.text = str(single_sheet.at[m,'OBA Number']) + '_' + str((single_sheet.at[m,'Sensor ID']))  #may be unecessary at this point
                wafer_batch.text = str(single_sheet.at[m,'OBA Number'])
                wafer_comment.text = "delivery Date" + str(single_sheet.at[m,'Delivery (date leaving HPK)'])
            #wafer_location.text = str(single_sheet.at[m,'Current location sensor']).replace("->", "to")
            wafer_manufacturer.text = "Hamamatsu-HPK"
            #populate Sensor tags
            sensor_type.text = ' '.join([thickness,"Si Sensor", density, "Full"])
            sensor_ID.text = wafer_ID.text + "_0"
            sensor_name.text = wafer_name.text + "_0"
            
            sensor_location.text = str(single_sheet.at[m,'Current location sensor'])
            if sensor_location.text.find('->') != -1:
               sensor_location.text = sensor_location.text.replace('->', 'to')    
            # sensor_institution.text = str(single_sheet.at[m,'Current location sensor'])
            sensor_manufacturer.text = wafer_manufacturer.text
            
            #check for -> in halfmoon. DB does not read '->' correctly 
            hmLocation = str(single_sheet.at[m,'Current location half moons'])
            if hmLocation.find('->') != -1:
                hmLocation = hmLocation.replace('->', 'to')

            halfmoon_TOP_type.text = ' '.join([thickness,"Si Sensor", density, "Halfmoon-T"])
            halfmoon_TOP_ID.text = wafer_ID.text + "_TOP"
            halfmoon_TOP_name.text = wafer_name.text + "_TOP"
            halfmoon_TOP_location.text = hmLocation
            halfmoon_TOP_manufacturer.text = wafer_manufacturer.text
                
            halfmoon_BOT_type.text = ' '.join([thickness,"Si Sensor", density, "Halfmoon-B"])
            halfmoon_BOT_ID.text = wafer_ID.text + "_BOT"
            halfmoon_BOT_name.text = wafer_name.text + "_BOT"
            halfmoon_BOT_location.text = hmLocation
            halfmoon_BOT_manufacturer.text = wafer_manufacturer.text
                
            halfmoon_UL_type.text = ' '.join([thickness,"Si Sensor", density, "Halfmoon-TL"])
            halfmoon_UL_ID.text = wafer_ID.text + "_TL"
            halfmoon_UL_name.text = wafer_name.text + "_TL"
            halfmoon_UL_location.text = hmLocation
            halfmoon_UL_manufacturer= wafer_manufacturer.text
                
            halfmoon_LL_type.text = ' '.join([thickness,"Si Sensor", density, "Halfmoon-BL"])
            halfmoon_LL_ID.text = wafer_ID.text + "_BL"
            halfmoon_LL_name.text = wafer_name.text + "_BL"
            halfmoon_LL_location.text = hmLocation
            halfmoon_LL_manufacturer.text = wafer_manufacturer.text
            
            halfmoon_LR_type.text = ' '.join([thickness,"Si Sensor", density, "Halfmoon-BR"])
            halfmoon_LR_ID.text = wafer_ID.text + "_BR"
            halfmoon_LR_name.text = wafer_name.text + "_BR"
            halfmoon_LR_location.text = hmLocation
            halfmoon_LR_manufacturer.text = wafer_manufacturer.text
            
            halfmoon_UR_type.text = ' '.join([thickness,"Si Sensor", density, "Halfmoon-TR"])
            halfmoon_UR_ID.text = wafer_ID.text + "_TR"
            halfmoon_UR_name.text = wafer_name.text + "_TR"
            halfmoon_UR_location.text = hmLocation
            halfmoon_UR_manufacturer.text = wafer_manufacturer.text
            
            #divide sensors into directory by batch for easier upload to dbloader
            outputDir = str(single_sheet.at[m,'OBA Number']) #Batch directory
            if not os.path.exists(outputDir):
                os.mkdir(outputDir)
            outputFile = outputDir + '/' + wafer_ID.text + '.xml'
            with open(outputFile, 'w') as f:
                wafertemplatetree.write(outputFile)



##MGW note, the 
# 
    

