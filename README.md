# part_xml_generator

## Name
HGCAL Sensors parts xml generator

## Description
Tool for converting Excel file (download from Google doc https://docs.google.com/spreadsheets/d/1OqYDyMihO1sLiaeoGhe51_hXFiQulFu5MlP6jf2RjLQ/edit#gid=0) to part XML files. Utilized for uploading silicon sensor parts to the HGCAL detector


## Installation
Download to script and the template
Pandas package is prerequisite. 

## Usage
python ./xlsx_to_xml_Parametrized.py [-h] [-n NSHEETS] [-d DEBUG] [-c CAMPAIGN] src template

src is path to Excel

template is path to XML template. Template can be found in the templates directory


